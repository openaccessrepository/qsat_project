QuantSat-PT is the first stepping stone to start developing satellite quantum communications in Portugal. It’s the first stage of a wider and longer-term vision, which will include sending our satellite to space by 2024 and implementing an autonomous proof-of-principle demonstration of space-ground quantum key distribution, of strategic significance for Portugal. 

The goals of project QuantSat-PT, corresponding to the first stage (2020-2021), are: 

1. To develop a quantum payload for space-earth quantum key distribution in 2U. 
2. To test the quantum payload on earth over a distance of several kilometres. 
3. To develop the preliminary design of the space segment. 

